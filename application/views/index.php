<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MERLO MAQUINAS</title>

    <!-- Bootstrap core CSS -->
    <link href=<?php echo "'" . base_url()?>vendor/bootstrap/css/bootstrap.min.css<?php echo "'"?> rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href=<?php echo "'" . base_url()?>css/modern-business.css<?php echo "'"?> rel="stylesheet">

  </head>

  <body>

    <?php include('navbar.php'); ?>

    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('img/lectorDeTarjetas.jpg')">
		    <div class="carousel-caption d-none d-md-block text-dark">
			 
              <h3>Lector de tarjetas</h3>
              <p>Nuevo lector de tarjetas.</p>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('img/balanzaIndustrial.jpg')">
            <div class="carousel-caption d-none d-md-block text-dark">
              <h3>Balanza industrial</h3>
              <p>Nuevo modelo reforzado.</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('img/lectorCodigoBarras.png')">
            <div class="carousel-caption d-none d-md-block text-dark">
              <h3>Lector de codigo de barras</h3>
              <p>Innovador diseño adaptable a todo tipo de superficies.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev " href="#carouselExampleIndicators" role="button" data-slide="prev">
		  <span class="carousel-control-next-icon text-dark " aria-hidden="true"></span>
          <span class="sr-only ">Siguiente</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon text-dark" aria-hidden="true"></span>
          <span class="sr-only">Previa</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">

	    <div class="row mb-4">
        <div class="col-md-10>
		
 
		     <div class="col-md-12>
				
			<h1 class="my-4 text-primary">30 Años en el mercado abalan nuestra experiencia</h1>
	  </div>
      
		</div>

      <h1 class="my-4">Productos destacados</h1>
	  
	   <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 portfolio-item">
          <div class="card h-20">
            <a href="#"><img class="card-img-top" src="img/img1.jpg" alt=""></a>
                <a href="#" class="btn btn-primary h-20">Balanzas</a>
          </div>
        </div>
	
        <div class="col-lg-2 col-md-2 col-sm-2 portfolio-item">
          <div class="card h-20">
            <a href="#"><img class="card-img-top" src="img/img1.jpg" alt=""></a>
                <a href="#" class="btn btn-primary h-20">Fiscales</a>
          </div>
        </div>

		   <div class="col-lg-2 col-md-2 col-sm-2 portfolio-item">
          <div class="card h-20">
            <a href="#"><img class="card-img-top" src="img/img1.jpg" alt=""></a>
                <a href="#" class="btn btn-primary h-20">Pos</a>
          </div>
        </div>
		   <div class="col-lg-2 col-md-2 col-sm-2 portfolio-item">
          <div class="card h-20">
            <a href="#"><img class="card-img-top" src="img/img1.jpg" alt=""></a>
                <a href="#" class="btn btn-primary h-20">Lectores</a>
          </div>
      </div>
	   <div class="col-lg-2 col-md-2 col-sm-2 portfolio-item">
          <div class="card h-20">
            <a href="#"><img class="card-img-top" src="img/img1.jpg" alt=""></a>
                <a href="#" class="btn btn-primary h-20">Armado de PC</a>
          </div>
      </div>
	  	   <div class="col-lg-2 col-md-2 col-sm-2 portfolio-item">
          <div class="card h-20">
            <a href="#"><img class="card-img-top" src="img/img1.jpg" alt=""></a>
                <a href="#" class="btn btn-primary h-20">Accesorios</a>
          </div>
      </div>
	  
</div>
</div>
   

      <!-- Call to Action Section -->
  
    </div>
    <!-- /.container -->
  <!--	
	  <footer class="py-5 bg-black">
      <div class="container">
		<img src="img/marca_01.jpg">
       	<img src="img/marca_02.jpg">
		<img src="img/marca_03.jpg">
		<img src="img/marca_04.jpg">
		<img src="img/marca_05.jpg">
		<img src="img/marca_06.jpg">
		<img src="img/marca_07.jpg">
		<img src="img/marca_08.jpg">
		<img src="img/marca_09.jpg">
		<img src="img/marca_10.jpg">
      </div>
	  
	  -->
    <footer class="py-5 bg-dark">
      <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; MyM Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>
    <!-- Footer -->
  
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src=<?php echo "'" . base_url()?>vendor/jquery/jquery.min.js<?php echo "'"?>></script>
    <script src=<?php echo "'" . base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js<?php echo "'"?>></script>

  </body>

</html>
