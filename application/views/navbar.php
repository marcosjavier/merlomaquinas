<!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php">MERLO MAQUINAS</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                    <a class="nav-link" href=<?php echo "'" . base_url()?>Welcome/index<?php echo "'"?>>Bienvenidos</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href=<?php echo "'" . base_url()?>Products/index<?php echo "'"?> id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Productos
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>folletos.php<?php echo "'"?>>Folletos</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>respuestos.php<?php echo "'"?>>Repuestos</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>armadodepc.php<?php echo "'"?>>Armado de PC</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href=<?php echo "'" . base_url()?>Products/index<?php echo "'"?> id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Controladores fiscales
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>impresorasFiscales.php<?php echo "'"?>>Impresoras fiscales</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>controladorasFiscales.php<?php echo "'"?>>Controladores fiscales</a>
              </div>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href=<?php echo "'" . base_url()?>Products/index<?php echo "'"?> id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Maquinas de oficina
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Impresoras de comandos o no fiscales</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Contadores de billetes</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Calculadoras para Oficinas y Comercio</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Lectores de Código de Barra</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Lectores de Cheques</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Destructores de Papeles</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Gavetas para Dinero</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Sillas</a>
              </div>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href=<?php echo "'" . base_url()?>Products/index<?php echo "'"?> id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Balanzas
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Industriales</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>De precisi&oacute;n</a>
              </div>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href=<?php echo "'" . base_url()?>Welcome/index<?php echo "'"?>>Computadoras</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href=<?php echo "'" . base_url()?>Products/index<?php echo "'"?> id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Insumos / accesorios
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Parlantes</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Mouse</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Memorias</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>UPS</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Cables</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Resmas</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>cds/dvds</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Camaras web</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Cartuchos</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Grabadoras</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Estuches/Sobres cds/dvs</a>
              </div>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href=<?php echo "'" . base_url()?>Products/index<?php echo "'"?> id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Consolas / accesorios
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>>Play Station</a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>></a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>></a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>></a>
                <a class="dropdown-item" href=<?php echo "'" . base_url()?>.php<?php echo "'"?>></a>
              </div>
            </li>
            
          </ul>
        </div>
      </div>
    </nav>