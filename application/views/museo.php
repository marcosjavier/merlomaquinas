<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MERLO MAQUINAS</title>

    <!-- Bootstrap core CSS -->
    <link href=<?php echo "'" . base_url()?>vendor/bootstrap/css/bootstrap.min.css<?php echo "'"?> rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href=<?php echo "'" . base_url()?>css/modern-business.css<?php echo "'"?> rel="stylesheet">

  </head>

  <body>

 <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.html">MERLO MAQUINAS</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.html">Bienvenidos</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Producto
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href="productos.html">Categorias</a>
                <a class="dropdown-item" href="folletos.html">Folletos</a>
                <a class="dropdown-item" href="respuestos.html">Repuestos</a>
                <a class="dropdown-item" href="armadodepc.html">Armado de PC</a>
      
              </div>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="museo.html">Museo Tecnologico</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contacto.html">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Museo Tecnologico</h1>
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_01_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a>OLIVETTI TEKNE 3</a>
              </h4>
            </div>
          </div>
         </div>
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_02_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a>UNDERWOOD 54</a>
              </h4>
            </div>
          </div>
         </div>
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_03_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a>OLIVETTI LETTERA 32</a>
              </h4>
            </div>
          </div>
         </div>
		 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_04_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a>OLIVETTI LETTERA 25</a>
              </h4>
            </div>
          </div>
         </div>
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_05_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a>REMINGTON</a>
              </h4>
            </div>
          </div>
         </div>	 
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_06_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a>OLIVETTI</a>
              </h4>
            </div>
          </div>
         </div> 
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_07_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a></a>
              </h4>
            </div>
          </div>
         </div> 
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_08_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a></a>
              </h4>
            </div>
          </div>
         </div> 
		 		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_09_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a></a>
              </h4>
            </div>
          </div>
         </div> 
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_10_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a></a>
              </h4>
            </div>
          </div>
         </div> 
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_11_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a></a>
              </h4>
            </div>
          </div>
         </div> 
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/museo/museo_12_ch.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a></a>
              </h4>
            </div>
          </div>
         </div> 
 </div>


    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; MyM Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src=<?php echo "'" . base_url()?>vendor/jquery/jquery.min.js<?php echo "'"?>></script>
    <script src=<?php echo "'" . base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js<?php echo "'"?>></script>

  </body>

</html>
