<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MERLO MAQUINAS</title>

    <!-- Bootstrap core CSS -->
    <link href=<?php echo "'" . base_url()?>vendor/bootstrap/css/bootstrap.min.css<?php echo "'"?> rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href=<?php echo "'" . base_url()?>css/modern-business.css<?php echo "'"?> rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.html">MERLO MAQUINAS</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.html">Bienvenidos</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Producto
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href="productos.html">Categorias</a>
                <a class="dropdown-item" href="folletos.html">Folletos</a>
                <a class="dropdown-item" href="respuestos.html">Repuestos</a>
                <a class="dropdown-item" href="armadodepc.html">Armado de PC</a>
      
              </div>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="museo.html">Museo Tecnologico</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contacto.html">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

   
   <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Contacto</h1>


      <!-- Content Row -->
      <div class="row">
        <!-- Map Column -->
        <div class="col-lg-8 mb-4">
          <!-- Embedded Google Map -->
          <iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.ar/maps?hl=es&amp;q=google+maps+Av.+Libertador+394+-+Merlo&amp;ie=UTF8&amp;hq=&amp;hnear=Av+Del+Libertador+394,+Merlo,+Buenos+Aires&amp;gl=ar&amp;ll=-34.670206,-58.725271&amp;spn=0.027672,0.059566&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
        </div>
        <!-- Contact Details Column -->
        <div class="col-lg-4 mb-4">
          <h3>Contacto Detalles </h3>
          <p>
			Av. Libertador 394 - Merlo CP (1722)
            <br>Buenos Aires - Argentina
            <br>
          </p>
          <p>
            <abbr title="telefono">Télefono</abbr>: (011) 0220-4823481
          </p>
          <p>
            <abbr title="Email">Mail</abbr>:
            <a href="info@merlomaquinas.com.ar">info@merlomaquinas.com.ar
            </a>
          </p>
        </div>
      </div>
      <!-- /.row -->

      <!-- Contact Form -->
      <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
      <div class="row">
        <div class="col-lg-6 mb-4">
          <h3>Contactenos</h3>
          <form name="sentMessage" id="contactForm" novalidate>
            <div class="control-group form-group">
              <div class="controls">
                <label>Nombre Completo:</label>
                <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                <p class="help-block"></p>
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Teléfono de contacto:</label>
                <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Mail:</label>
                <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
              </div>
            </div>
			</div>
			  <div class="col-lg-6 mb-3">
            <div class="control-group form-group">
              <div class="controls">
                <label>Mensaje:</label>
                <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
              </div>
            </div>
			 
            <div id="success" class=" col-md-6 col-md-offset-3"></div>
            <!-- For success/fail messages -->
            <button type="submit" class="btn btn-primary" id="sendMessageButton">Enviar</button>
			</div>
			</div>
          </form>
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; MyM Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src=<?php echo "'" . base_url()?>vendor/jquery/jquery.min.js<?php echo "'"?>></script>
    <script src=<?php echo "'" . base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js<?php echo "'"?>></script>

    <!-- Contact form JavaScript -->
    <!-- Do not edit these files! In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <script src=<?php echo "'" . base_url()?>js/jqBootstrapValidation.js<?php echo "'"?>></script>
    <script src=<?php echo "'" . base_url()?>js/contact_me.js<?php echo "'"?>></script>

  </body>

</html>
