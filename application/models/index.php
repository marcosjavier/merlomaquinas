<?php

class Index extends MY_Model {

	public function getMenu(){
		$sql = "SELECT m.menu,m.referencia,m.target
				FROM menu m 
				WHERE m.inView = 1";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
}