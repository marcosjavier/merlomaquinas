<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('parser');
		$this->load->model('index','model');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('index', $datos);
	}

	public function museotecnologico()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('museo', $datos);
	}

	public function controladoresfiscales()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('controladoresfiscales', $datos);
	}

	public function maquinasdeoficina()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('index', $datos);
	}

	public function balanzas()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('index', $datos);
	}

	public function insumosaccesorios()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('insumos', $datos);
	}

	public function consolasaccesorios()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('insumos', $datos);
	}

	public function folletos()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('folletos', $datos);
	}

	public function contacto()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('contacto', $datos);
	}

	public function dondeestamos()
	{
		$datos= array(
			'menu' => $this->model->getMenu(),
		);
		$this->parser->parse('index', $datos);
	}
}
